#!/bin/bash

:<<EOF
#生成单个单表单列SQL文件
#定义变量
randgen_path=/home/jing/下载/randgen-master
filezz_path=/home/jing/tmp/test/test.zz
fileyy_path=/home/jing/tmp/test/test.yy
db_info=dbi:mysql:host=localhost:port=3306:user=root:password=abc123:database=test1
all_output_file=/home/jing/tmp/all_output.log
sqlfile_path=/home/jing/tmp/single_tab_col_int.sql
#建表，插入初始数据.zz文件./home/jing/tmp/test/test.zz
cd $randgen_path
perl gentest.pl --dsn=$db_info --gendata=$filezz_path --grammar=$fileyy_path --sqltrace=MarkErrors --queries=100  > $all_output_file 2>&1
cat $all_output_file | grep ^[^#] | tail -n +2 | head -n 557 |awk '{$1="";print $0}'|sed '4,6d;/COMMIT/d;/ALTER/d;/USE/d' > $sqlfile_path
cat $sqlfile_path | head -n 1 >> $sqlfile_path
#sed '1d' $sqlfile_path
EOF

#批量生成单表单列SQL文件
#定义变量
randgen_path=/home/jing/下载/randgen-master
filezz_path=/home/jing/tmp/test/test.zz
fileyy_path=/home/jing/tmp/test/test.yy
db_info=dbi:mysql:host=localhost:port=3306:user=root:password=abc123:database=test
all_output_file=/home/jing/tmp/all_output.log
sqlfile_path=/home/jing/tmp/sqlfile_tab
data_type=('bit' 'tinyint' 'bool' 'boolean' 'smallint' 'mediumint' 'int' 'integer' 'bigint' 'decimal' 'dec' 'float' 'double' 'date' 'time' 'datetime' 'timestamp' 'year' 'char' 'varchar' 'binary' 'blob' 'tinyblob' 'mediumblob' 'longblob' 'text' 'tinytext' 'mediumtext' 'longtext' 'enum' 'set')
cd  $randgen_path

for col_type in ${data_type[@]}
do
#修改.zz文件，创建不同数据类型的表
if [ $col_type = date ] || [ $col_type = time ] || [ $col_type = datetime ] || [ $col_type = timestamp ] || [ $col_type = year ]
then
	sed -i "13c temporals=>['$col_type']" $filezz_path
else
	sed -i "13c number=>['$col_type']" $filezz_path
fi
sed -i "8c types=>['$col_type']," $filezz_path

perl gentest.pl --dsn=$db_info --gendata=$filezz_path --grammar=$fileyy_path --sqltrace=MarkErrors --queries=100  > $all_output_file 2>&1
col_name=$(cat $all_output_file | grep ^[^#]| awk '$1 !~ /[0-9]/{print $1}')
cat $all_output_file | grep ^[^#] | tail -n +2 | head -n 557 |awk '{$1="";print $0}'|sed '4,6d;/COMMIT/d;/ALTER/d;/USE/d'| sed -e "2 a\ $col_name" | sed -e "1 a\ set session sql_mode='';" | sed -e '5 s/;/ENGINE=InnoDB;/g' > $sqlfile_path/single_tab_col_$col_type.test
cat $sqlfile_path/single_tab_col_$col_type.test | head -n 1 >> $sqlfile_path/single_tab_col_$col_type.test
done



