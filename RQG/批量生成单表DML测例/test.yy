query: 
	select | insert | update | delete ;

select: 
	SELECT all_distinct select_list FROM table_in_select AS X where_condition group_by order_by limit;

all_distinct: 
	    | 
	    ALL | 
	    DISTINCT |
	    DISTINCTROW ;
      
select_list: 
	* | _field[invariant] ; 

table_in_select: 
		_table  | _table  | _table   | _table  | 
		(SELECT select_list FROM _table) ;

where_condition: 
		|
		where | subquery | join where | union ;

where: 	
	WHERE X._field[invariant] arithmetic_operator value| 
	WHERE X._field[invariant] arithmetic_operator value|
	WHERE X._field[invariant] arithmetic_operator value|
	WHERE X._field[invariant] arithmetic_operator value|
	WHERE X._field[invariant] BETWEEN _digit[invariant] AND _digit[invariant] + 1;

arithmetic_operator:
	= | > | < | != | <> | <= | >= | like;

subquery: 
	WHERE X._field[invariant] IN ( SELECT _field[invariant] FROM _table AS B WHERE B._field = value) ;

join:  
	 INNER JOIN _table AS Y ON ( X . _field[invariant] = Y . _field[invariant] ) |
	 LEFT  JOIN _table AS Y ON ( X . _field[invariant] = Y . _field[invariant] ) |
	 RIGHT JOIN _table AS Y ON ( X . _field[invariant] = Y . _field[invariant] ) ;

union: UNION SELECT _field[invariant] FROM _table as Y ;

insert:
	INSERT ignore INTO  _table ( _field ) VALUES ( value ) |
	INSERT ignore INTO  _table ( _field ) VALUES ( value ) |
	INSERT ignore INTO  _table ( _field ) VALUES ( value ) |
	INSERT ignore INTO  _table ( _field ) VALUES ( value ) |
	INSERT ignore INTO  _table (_field) select ;
#INSERT ignore INTO _table TABLE _table ;

ignore:
	| | | | | | | | | IGNORE;

update: 
	UPDATE low_priority _table AS X SET _field = value  where order_by limit ;

low_priority:
	     | | | | | | | | |LOW_PRIORITY;

delete:
	DELETE low_priority quick ignore FROM _table AS X where order_by limit ;

quick:
	| | | | | | | | |QUICK ;

group_by:  
	| 
	GROUP BY X._field[invariant] having;

having:
	|
	HAVING X._field arithmetic_operator value ;

order_by: 
	| 
	ORDER BY X._field ;

limit:
     |
     LIMIT _digit ;

value: 
	_string | _digit | _string | _digit | _bigint | _date | _time | _datetime | _timestamp |;


