## 批量创建单表DML测试用例

#### 1. 要求

1. 单表单列，包含所有数据类型。
2. 测试用例结构
   1. 建表语句   create table...
   2. 插入初始化数据   insert into...
   3. 随机SQL语句    select   insert  update  delete
   4. 删除测试表   drop  table

#### 2. 过程

1. 调整zz文件，创建表，初始化。 可参考文件**test.zz**
2. 调整yy文件，尽量增加随机生成的SQL语句复杂度。可参考文件**test.yy**
3. 根据RQG产生的输出，抓取测试用例文件中所需的内容，生成一个完整的测例文件。可参考脚本**sql.sh**

#### 3.结果

​	将生成的测例文件，使用mysql-test测试工具进行测试。测例文件可参考**single_table_testcase**文件夹中的文件