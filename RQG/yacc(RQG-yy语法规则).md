# yacc入门

## 1. 文件格式

​		yacc源程序由三部分组成，各部分以“%%”为分隔符。说明部分和程序部分可选，规则部分是必需的。

​		完整的规定文件：

```
[说明部分]   
%%
规则部分
%%
[程序部分]
```

​		 最小的合法Yacc规定文件：

```  	
%%
规则部分
```

## 2. 各部分说明

  1. 说明部分	

     ​	C语言代码部分：

     ​		%{

     ​		头文件表：#include...  

     ​		宏定义：#define 标识符 字符串

     ​		数据类型定义

     ​		全局变量定义

     ​		%}

     ​	Yacc说明部分：

     ​		文法开始符定义  %start 非终结符

     ​		语义值类型定义  %union

     ​		终结符定义   %token

     ​		非终结符定义  %type

     ​		优先级和结合性定义  %left  %right  和  %nonassoc

  2. 规则部分

       1. 语法规则如下格式

          `A       ：    BODY   {/* action */}     ; `

     ​			  A :      表示一个非终结符名字。非终结符是可以被取代的。一个文法中必须有一个起始符号；这个起始符号属于非终结符的集合。

     ​				         yacc语法规则部分符合上下文无关文法，每个推导规则的左边只能有一个非终结符而不能有两个以上的非终结符或终结符。

     ​			BODY : 根据语法规则对A进行描述的非终结符和终结符

     ​			  {} :       如果要匹配特定序列的话，BODY后面可以跟随要执行的动作，这部分是可选的

     ​	   若一些文法规则有相同的左部，可以使用竖杠“|”来整合，避免重复写左部。		

     ```  
      A     :   B    C   D   ;
      A     :    E    F   ;
      A     :    G   ;
     ```

     ​			可以写作

      `A    :   B   C   D  |   E  F |  G   ;`

     ​		举例

     ``` sql
     query:
      	update | insert | delete ;
     
     update:
      	UPDATE _table SET _field = digit WHERE condition LIMIT _digit ;
     
     delete:
     	DELETE FROM _table WHERE condition LIMIT _digit ;
     
     insert:
     	INSERT INTO _table ( _field ) VALUES ( _digit ) ;
     
     condition:
      	_field < digit | _field = _digit ;
     ```

  3. 程序部分

     根据实际需求，符合C语言语法，实现相应功能

## 3. yy文件

yy文件的书写规则，与上述规则部分相同。

需要根据实际的SQL语法树，转换为上述格式。

非终结符使用小写字母表示，终结符即SQL语法中的关键字使用大写字母表示