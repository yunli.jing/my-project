## RQG.zz文件使用指导

1. 使用环境：

   ​	系统版本：Ubuntu18.04

   ​	数据库版本：mysql8.0.21

   ​	RQG版本：randgen-2.2.0

2. 文件用途

   ​	.zz文件用来准备测试数据，创建表和插入数据

3. 书写规则

   1. **$table**

      该部分主要定义表的大小，名字，存储引擎，字符集等表层级参数设置，以此来创建表结构

      书写规则格式：

      ``` 
      $tables={parameter => [],
      
      				   parameter => [],
      				   
      				   ...
      };
      ```

      parameter（参数说明）:

      ​		1) rows ：       要创建的表的大小。

      ​							 	默认是0,1,2,10,100，在不定义names参数时，会创建table0，table1...table100五个表（即在不设置其他参数的前提下

      ​								 rows里指定多少个值，就会创建多少个表。每个表的大小与rows中的值一一对应）

      ​		2) engines：   使用的存储引擎。

      ​								  如果不指定则使用MySQL服务器的默认引擎

      ​		3) partitions：要使用的分区子句。例如：partitions => [ undef,'KEY (pk) PARTITIONS 2',... ]

      ​								  为避免完全分区，可从配置文件中删除此子句。 要创建一些未分区的表，使用undef作为数组中的元素。

      ​		4) names：     用于生成表的表名。

      ​							 	如果表名多于名称，则当提供的用户定义名称用完时，将使用默认名称（即当rows参数不指定值时,先使用

      ​						     	names指定的表名,其余会使用table默认大小\_引擎_分区等的组合为表名；当rows参数指定值时，先使用names指定的表名，其余

      ​						    	 会以 table指定大小\_引擎_分区等的组合为表名，如下**例1**）。table\<rows>\_[engine]_[charset]_[collation]_[partition]\_\<pk>_\<row-format>

      ​		5) charsets：   指定字符集。例如：charsets => [ 'utf8','latin1',... ]

      ​		6) collations： 指定数据集如何排序，以及字符串的比对规则。 例如：collations => [ 'utf8_general_ci','latin1_swedish_ci',... ]

      ​						      	注意：将为字符集和排序规则的每种组合创建一个表，该表可能并不总是有效的。**例2**

      ​		7) pk：              指定要使用的类型。例如：pk => [ undef,'int auto_increment','int',... ]

      ​							 	 设置`pk => [ undef ]`时，创建的表没有主键 。若不指定则默认使用递增整数[ int auto_increment ]的序列。尚不支持生成非整数主键。

      ​		8) views：        指定要为每个基本表创建哪些视图。例如 `views => ['ALGORITHM=MERGE', 'ALGORITHM=TEMPTABLE',...]`

      ​		9) merges：     描述要创建的合并表。

      ​		  				        将在MyISAM或没有特定引擎的所有基础表上创建一个合并表（默认情况下，将使用MyISAM创建这些合并表）

       **例1**：

      ```	sql
      $tables={
      
      	names => ['example1','example2']
      	
      };
      ```

      ​	 			结果：# Creating MySQL table : example1，example2，table2，table10，table100

      ``` sql
      $tables={
      
      	rows => [15,20,25,30]
      	
      	names => ['example1','example2']
      	
      };
      ```

      ​				结果：# Creating MySQL table : example1，example2，table25，table30

       **例2**：(解释为什么建表不总是有效的原因)

      ``` 
      $tables={
      	rows =>[10]
      	charsets => ['utf8','latin1'],
      	collation => ['utf8_swedish_ci']
      };
      ```

      ​				结果：#Creating MySQL table : table10_utf8_utf8_swedish_ci   

      ​			  			 #Creating MYSQL table : table10_latin1_utf8_swedish_ci  失败

      ​			 			  CHARACTER SET latin1 COLLATE utf8_swedish_ci  failed: 1253 COLLATION 'utf8_swedish_ci' is not valid for CHARACTER SET 'latin1'

      

   2. **$field**

      该部分主要定义表中每一列的数据类型、索引、字符集等列层级的参数设置，以此来创建满足各个条件的列

      ``` 
      $fields={parameter=>[], 
      
      			   parameter=>[],
      
      			   ...
      
       };
      ```

      parameter（参数说明）：

      ​		 1) types ：    指定列的数据类型。

      ​						         可以使用任何MySQL类型名称、长度修饰符，例如 char(50)。 如果未指定长度，则将char列创建为char(1)。 set和

      ​						         enum字段类型默认接受从A到Z的每个字母。，这样可以在该列填充随机字母。 types选项的默认值为int,varchar,date,time,datetime

      ​								 https://dev.mysql.com/doc/refman/8.0/en/data-types.html可以查看mysql所有data-types

      ​		 2) indexes ：指定字段上是否有索引。 

      ​							     undef，表示没有索引；index，表示标准索引；unique，表示唯一索引；fulltext，表示全文索引。

      ​						         注意:某些存储引擎可能不支持所有索引类型，因此表创建可能会失败。 每个字段和索引都是分别创建的，因此目前不支持复合索引。 

      ​		3) null ：        对插入数据的约束,是否允许插入数据为NULL。有效值：undef,null,not null

      ​		4) default ：  在插入数据时,如果这个字段值缺省的时候所插入的默认值,

      ​		5) sign ：        对于数字字段类型，指定该字段是带符号的还是无符号的。有效值是unsigned，signed和undef ,默认设置是仅生成signed字段。    

      ​		6) charsets ：对于字符串字段类型，将为列出的每个字符集创建字段。默认是undef，表示不指定字符集

      ​		7) collation ：对于字符串字段类型，将为列出的每个排序规则创建字段

   3. **$data**	

      该部分主要定义将哪些数据插入不同字段类型的列中，为后续测试做准备

      ``` 
      $data={parameter=>[], 
      
      			  parameter=>[],
      
      			 ...
      
      };
      ```

      parameter（参数说明）:

      ​		1) numbers：为所有整数、浮点数类型字段生成值。

      ​							    [null]  在该列允许为null时，插入null值

      ​							    [digit] 为该列插入0-9单个随机数字

      ​						    	[undef] 为该列插入一个合适的随机值

      ​							    [boolean] 布尔值，随机插入0,1，等等					

      ​		 2) strings ：为所有字符串类型的列生成值。

      ​						       [null] 空值

      ​						       [letter]  A-Z的随机字母

      ​					 	      [english] 从100个最常用的英语单子列表中选择一个随机单词

      ​					 	      [string，string(N)]  生成一个随机字符串，其中包含字母A-Z，长度从1-N个字符。若未指定N，则生成单个随机字母

      ​					  	     [ascii,ascii(N) ] 生成一个随机字符串，其中包含1-N个字符的ascii字符。若未指定N，则生成一个随机的ASCII字符

      ​							   [empty]  空字符串

      ​							   ['自定义字符串']  例如：string => ['abdc','dirdir'] ,生成随机数据为‘abdc’和‘dirdir‘，比例为1:1

      ​		 3) blobs：   为blob类型的列生成值

      ​							   blobs     => [ 'english', 'data' ]

      ​	    4) temporals ：为时间类型的列生成值

       			                      temporals => [ undef , 'null' , 'time' , 'date' , 'datetime' , 'year(2)' , 'year(4)' , 'timestamp' ]		       

4. 举例说明：

   example.zz

   ``` 
   $tables = {
           rows => [0, 1, 10, 100],
           partitions => [ undef , 'KEY (pk) PARTITIONS 2' ]
   };
   
   $fields = {
           types => [ 'int', 'char', 'enum', 'set' ],
           indexes => [undef, 'key' ],
           null => [undef, 'not null'],
           default => [undef, 'default null'],
           sign => [undef, 'unsigned'],
           charsets => ['utf8', 'latin1']
   };
   
   $data = {
           numbers => [ 'digit', 'null', undef ],
           strings => [ 'letter', 'english' ],
           blobs => [ 'data' ],
   	    temporals => ['date', 'year', 'null', undef ]
   }
   ```

   建表语句：
   
   ```sql
   mysql> show create table table100_key_pk_parts_2_int_autoinc\G
   *************************** 1. row ***************************
          Table: table100_key_pk_parts_2_int_autoinc
   Create Table: CREATE TABLE `table100_key_pk_parts_2_int_autoinc` (
     `col_int_unsigned_not_null_key` int unsigned NOT NULL,
     `col_enum_latin1` enum('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z') CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
     `col_set_utf8` set('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
     `col_set_latin1` set('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z') CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
     `col_int` int DEFAULT NULL,
     `col_char_latin1_not_null` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
     `col_char_utf8_not_null` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
     `col_enum_latin1_not_null` enum('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
     `col_enum_latin1_not_null_key` enum('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
     `col_char_utf8` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
     `pk` int NOT NULL AUTO_INCREMENT,
     `col_char_utf8_not_null_key` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
     `col_int_unsigned` int unsigned DEFAULT NULL,
     `col_set_latin1_key` set('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z') CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
     `col_enum_utf8_not_null` enum('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
     `col_char_utf8_key` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
     `col_int_key` int DEFAULT NULL,
     `col_enum_latin1_key` enum('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z') CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
     `col_char_latin1_key` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
     `col_char_latin1` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
     `col_int_not_null_key` int NOT NULL,
     `col_set_utf8_key` set('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
     `col_enum_utf8_key` enum('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
     `col_int_not_null` int NOT NULL,
     `col_enum_utf8` enum('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
     `col_enum_utf8_not_null_key` enum('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
     `col_set_utf8_not_null_key` set('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
     `col_char_latin1_not_null_key` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
     `col_set_latin1_not_null_key` set('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
     `col_int_unsigned_not_null` int unsigned NOT NULL,
     `col_set_latin1_not_null` set('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
     `col_set_utf8_not_null` set('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
     `col_int_unsigned_key` int unsigned DEFAULT NULL,
     PRIMARY KEY (`pk`),
     KEY `col_int_unsigned_not_null_key` (`col_int_unsigned_not_null_key`),
     KEY `col_enum_latin1_not_null_key` (`col_enum_latin1_not_null_key`),
     KEY `col_char_utf8_not_null_key` (`col_char_utf8_not_null_key`),
     KEY `col_set_latin1_key` (`col_set_latin1_key`),
     KEY `col_char_utf8_key` (`col_char_utf8_key`),
     KEY `col_int_key` (`col_int_key`),
     KEY `col_enum_latin1_key` (`col_enum_latin1_key`),
     KEY `col_char_latin1_key` (`col_char_latin1_key`),
     KEY `col_int_not_null_key` (`col_int_not_null_key`),
     KEY `col_set_utf8_key` (`col_set_utf8_key`),
     KEY `col_enum_utf8_key` (`col_enum_utf8_key`),
     KEY `col_enum_utf8_not_null_key` (`col_enum_utf8_not_null_key`),
     KEY `col_set_utf8_not_null_key` (`col_set_utf8_not_null_key`),
     KEY `col_char_latin1_not_null_key` (`col_char_latin1_not_null_key`),
     KEY `col_set_latin1_not_null_key` (`col_set_latin1_not_null_key`),
     KEY `col_int_unsigned_key` (`col_int_unsigned_key`)
   ) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
   /*!50100 PARTITION BY KEY (pk)
   PARTITIONS 2 */
   1 row in set (0.00 sec)
   ```
   
