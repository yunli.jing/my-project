### RQG(Random Query Generator )使用教程

随机查询生成器是用于通过随机生成的数据和查询测试MySQL服务器的工具。 RQG又名randgen。

1. 安装环境

   ​	Ubuntu18.04 

2. 安装前提

   ​	1)  安装perl

   ​	`sudo apt install perl `

   ​	2)  安装cpan

   ​	`sudo apt install cpan`

   ​	3)  安装DBD::mysql 

​	    	`sudo perl -MCPAN -e 'install DBD::mysql'`

​			4)  安装libdbi-perl

​			` sudo  apt  install libdbi-perl`

3. 下载randgen-2.2.0.tgz

​      	[下载地址](https://launchpad.net/randgen/+download) 

4. 解压randgen

​		`tar -xzf  randgen-2.2.0.tgz `

​		如果想要创建自己的RQG框架工作副本，可执行  `~/$ bzr branch lp:randgen` ,会在当前目录下创建文件名为randgen的副本目录

5. 使用,创建一个测试

   `~/randgen-2.2.0$ perl gentest.pl --dsn=dbi:mysql:host=localhost:port=3306:user=root:database=test:password=123123 --gendata=conf/example.zz  --grammar=conf/example.yy`

   --dsn:描述要连接的服务器的信息，它是DBI DSN的标准perl格式

   --gendata:指定数据生成器的配置文件，example.zz文件内为准备测试数据，创建表和数据的规则

   --grammar:指定生成查询时要使用的语法文件，example.yy文件内容为测试要执行的随机查询语句的语法规则

6. 报错

   执行上述测试时，可能会出现如下报错，这是perl自身存在的bug导致	

```
Can't use 'defined(@array)' (Maybe you should just omit the defined()?) at lib/GenTest/Properties.pm line 168.
Compilation failed in require at gentest.pl line 27.
BEGIN failed--compilation aborted at gentest.pl line 27.
```

​	解决方法：

​	`		jing@jing-OptiPlex-5070:~/下载/randgen-2.2.0$ cat -n lib/GenTest/Properties.pm | grep 168`
​		 168	        join(", ", map {"'--".$_."'"} sort @illegal). ". " if defined @illegal;

​	vim 将if defined @illegal 改为if @illegal

