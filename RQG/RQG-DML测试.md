#### RQG-DML测试

测试环境：Ubuntu18.04   mysql8.0.21  randgen(http://github.com/MariaDB/randgen)

1. 准备数据

   1. 执行语句：`perl gendata.pl --dsn=dbi:mysql:host=localhost:port=3306:user=root:password=123123:database=test  --spec=conf/examples/ztmp_test.zz `

   2. 执行结果

      ``` 
      # 2020-09-02T17:34:48 [29020] Using Log::Log4perl
      # 2020-09-02T17:34:48 [29020] Creating MySQL table: test.table0_int_autoinc; engine: ; rows: 0 .
      # 2020-09-02T17:34:48 [29020] Creating MySQL table: test.table0_int; engine: ; rows: 0 .
      # 2020-09-02T17:34:48 [29020] Creating MySQL table: test.table0_key_pk_parts_2_int_autoinc; engine: ; rows: 0 .
      # 2020-09-02T17:34:49 [29020] Creating MySQL table: test.table0_key_pk_parts_2_int; engine: ; rows: 0 .
      # 2020-09-02T17:34:49 [29020] Creating MySQL table: test.table1_int_autoinc; engine: ; rows: 1 .
      # 2020-09-02T17:34:49 [29020] Creating MySQL table: test.table1_int; engine: ; rows: 1 .
      # 2020-09-02T17:34:49 [29020] Creating MySQL table: test.table1_key_pk_parts_2_int_autoinc; engine: ; rows: 1 .
      # 2020-09-02T17:34:49 [29020] Creating MySQL table: test.table1_key_pk_parts_2_int; engine: ; rows: 1 .
      # 2020-09-02T17:34:49 [29020] Creating MySQL table: test.table10_int_autoinc; engine: ; rows: 10 .
      # 2020-09-02T17:34:49 [29020] Creating MySQL table: test.table10_int; engine: ; rows: 10 .
      # 2020-09-02T17:34:49 [29020] Creating MySQL table: test.table10_key_pk_parts_2_int_autoinc; engine: ; rows: 10 .
      # 2020-09-02T17:34:49 [29020] Creating MySQL table: test.table10_key_pk_parts_2_int; engine: ; rows: 10 .
      # 2020-09-02T17:34:49 [29020] Creating MySQL table: test.table100_int_autoinc; engine: ; rows: 100 .
      # 2020-09-02T17:34:49 [29020] Creating MySQL table: test.table100_int; engine: ; rows: 100 .
      # 2020-09-02T17:34:50 [29020] Creating MySQL table: test.table100_key_pk_parts_2_int_autoinc; engine: ; rows: 100 .
      # 2020-09-02T17:34:50 [29020] Creating MySQL table: test.table100_key_pk_parts_2_int; engine: ; rows: 100 .
      ```

   3. 执行脚本：

      ```
      $tables={
              rows => [0,1,10,100],
              partitions => [undef,'key(pk) partitions 2 '],  
              pk => ['int auto_increment','int']
      };
      
      $fields={
              types => [ 'int', 'char(5)', 'enum', 'set' ],
              indexes => [undef,'key']
      };
      
      $data={
              numbers => [undef,'digit'],
              strings => ['letter','english']
      };
      ```

   4. 其他：尝试创建复杂分区表，如含有子分区表时

      1. `partitions => [undef,'key(pk) subpartition by key(pk) subpartitions 2']`

         **failed: 1500 It is only possible to mix RANGE/LIST partitioning with HASH/KEY partitioning for subpartitioning**

      2. `partitions => [undef,'rangen(pk) subpartition by key(pk) subpartitions 2']`

         **ERROR 1492 (HY000): For RANGE partitions each partition must be defined**

      3. `partitions => [undef,'rangen(pk) subpartition by key(pk) subpartitions 2 (partition p0 values less then (5),partition p1 values less then maxvalue )']`

         **failed: 1059 Identifier name 'table0_range_pk_subpart_by_key_pk_subparts_2_part_p0_vaules_less_then_5_part_p1_values_less_then_m' is too long**

      4. 总结：尝试创建复杂分区表失败，如何修改表名长度限制或者重新定义表名命名规则还有待研究。

2. 测试过程

   1. 执行语句：

      `perl gentest.pl --dsn=dbi:mysql:host=localhost:port=3306:user=root:password=abc123:database=test --grammar=conf/examples/ztmp_test.yy --sqltrace > conf/examples/all.log 2>&1`

      有效SQL：`cat conf/examples/all.log | grep ^\[^#] > conf/examples/sql.log `

      无效SQL及错误原因：`cat conf/examples/all.log | grep ErrorFilter > conf/examples/error.log `

   2. 执行脚本

      ```
      query:
              select | insert | update | delete ;
      
      select:
              SELECT select_list FROM  join_on  where_condition group_by order_by limit;
      
      select_list:
              * | _field[invariant] ;
      
      join_on:
              _table AS X partition|
              _table AS X partition INNER JOIN _table AS Y partition ON ( X . _field[invariant] = Y . _field[invariant] ) |
              _table AS X partition LEFT  JOIN _table AS Y partition ON ( X . _field[invariant] = Y . _field[invariant] ) |
              _table AS X partition RIGHT JOIN _table AS Y partition ON ( X . _field[invariant] = Y . _field[invariant] ) ;
      
      insert:
              INSERT INTO _table partition ( _field,_field) VALUES ( value,value ) ;
      
      update:
              UPDATE _table AS X SET _field = value  where_condition order_by limit ;
      
      delete:
              DELETE FROM _table AS X partition  where_condition limit ;
      
      partition:
               |
               PARTITION (p0)|
               PARTITION (P1);
      
      where_condition:
                      |
                      WHERE X._field[invariant] > value |
                      WHERE X._field[invariant] < value |
                      WHERE X._field[invariant] = value ;
      
      group_by:
              |
              GROUP BY X._field[invariant] having;
      
      having:
              |
              HAVING X._field > value |
              HAVING X._field < value |
              HAVING X._field = value ;
      
      order_by:
              |
              ORDER BY X._field ;
      
      limit:
           |
           LIMIT _digit ;
      ```

3. 测试结果

   根据调整zz文件，可创建多个结构不同的表并插入指定行数的初始化数据	

   根据调整yy文件，可随机生成多条SQL语句，尽量减少输出中的SQL syntax error